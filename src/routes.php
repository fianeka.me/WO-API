<?php

use Slim\Http\Request;
use Slim\Http\Response;
require_once 'DbHelper.php';
include 'paramchecker.php';

// Routes
$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    $this->logger->info("Slim-Skeleton '/' route");
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post("/users", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $result = $helper->getUsers();
      return $response->withJson($result, 200);
});

$app->post("/login", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('nik', 'password'));
      if (empty($missing)) {
        $result = $helper->userLogin($nik,$password);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});
$app->post("/profile", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('nik'));
      if (empty($missing)) {
        $result = $helper->getProfile($nik);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});

$app->post("/jenisorder", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('idsub'));
      if (empty($missing)) {
        $result = $helper->getJenisOrder($idsub);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});

$app->post("/barangatk", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('idkategori'));
      if (empty($missing)) {
        $result = $helper->getBarang($idkategori);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});

$app->post("/historybon", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('nik'));
      if (empty($missing)) {
        $result = $helper->getHistory("bon",$nik);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});

$app->post("/historyorder", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('nik'));
      if (empty($missing)) {
        $result = $helper->getHistory("order",$nik);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});

$app->post("/orderdetail", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('nik','idorder'));
      if (empty($missing)) {
        $result = $helper->getDetailOrder($nik,$idorder);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});

$app->post("/bon", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('nik','jumlah','idatk'));
      if (empty($missing)) {
        $result = $helper->addBon($idatk,$jumlah,$nik);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});

$app->post("/order", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('nik','idjenis','idsub',"keterangan"));
      if (empty($missing)) {
        $result = $helper->addOrder($idjenis,$idsub,$nik,$keterangan);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});

$app->post("/password", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $req = extract($request->getParsedBody());
      $missing = cekRequired(array('nik','old_password','new_password',"confirm_password"));
      if (empty($missing)) {
        $result = $helper->changePassword($nik,$old_password,$new_password,$confirm_password);
        return $response->withJson($result, 200);
      }else{
        return $response->withJson($missing, 400);
      }
});


$app->post("/subdiv", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $result = $helper->getSubdiv();
      return $response->withJson($result, 200);
});

$app->post("/kategori", function (Request $request, Response $response){
      $helper = new DbHelper($this->db);
      $result = $helper->getKategori();
      return $response->withJson($result, 200);
});
