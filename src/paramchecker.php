<?php
function cekRequired($required_fields)
{

    $error = false;
    $error_fields = "";
    $request_params = $_REQUEST;
    $response = array();
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
    if ($error) {
        $response["result"] = false;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        return $response;
    }
    return $response;
}
?>
