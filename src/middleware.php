<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
$app->add(function ($request, $response, $next) {
    $getres = $request->getQueryParam("demo");
    if ($getres==true) {
      return $response = $next($request, $response);
    }
    $posres = $request->getParsedBody();
    $key = $posres['key'];
    if(!isset($key)){
      return $response->withJson(["result" => false, "message" => "API Key required"], 401);
    }

    if($key == "assi"){
      return $response = $next($request, $response);
    }

	return $response->withJson(["result" => false, "message" => "Wrong Api Key"], 401);
});
