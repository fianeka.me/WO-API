<?php
define('IMGPATH', 'http://192.168.43.47/wo/assets/'); // mobile version
// define('IMGPATH', 'http://10.0.3.2/wo/assets/'); // mobile version
class DbHelper
{
    private $con;
    function __construct($db)
    {
        $this->con = $db;
    }

    public function imagePath($data,$key,$folder)
    {
      $da = $data;
      foreach ($da as $k => $v) {
        $da[$k][$key] = IMGPATH.$folder."/".$v[$key];
      }
      return $da;
    }
    //login user
    public function userLogin($username,$password){
        $pass = md5($password);
        $stmt = $this->con->prepare("SELECT * FROM users WHERE username=:usr and password=:psd and type!=3");
        $stmt->execute(array(':usr'=>$username,':psd'=>$pass));
        $num_rows = $stmt->rowCount();
        $res = array();
        if ($num_rows>0) {
          $stmt = $this->con->prepare("SELECT * FROM karyawan WHERE nik=:nik");
          $stmt->execute(array(':nik' => $username));
          $res['result'] = true;
          $res['message'] = "Berhasil Login";
          $res['data'] = $stmt->fetchAll();
        }else{
          $res['result'] = false;
          $res['message'] = "Invalid Username & Password";
          }
        return $res;
    }
    public function changePassword($nik,$old_password,$new_password,$confirm_password){
        $old = md5($old_password);
        $stmt = $this->con->prepare("SELECT * FROM users WHERE username=:usr");
        $stmt->execute(array(':usr'=>$nik));
        $data = $stmt->fetchAll();
        $res = array();
        if ($new_password==$confirm_password) {
          if ($old==$data[0]['password']) {
            $stmt = $this->con->prepare("UPDATE users SET password=:psd WHERE username=:nik");
            $stmt->execute(array(':nik' => $nik, ':psd'=>md5($new_password)));
            $res['result'] = true;
            $res['message'] = "Password Berhasil Dirubah";
          }else{
            $res['result'] = false;
            $res['message'] = "Password Lama Tidak Sesuai";
          }
        }else{
          $res['result'] = false;
          $res['message'] = "Password Baru Tidak Ada Kecocokan";
          }
        return $res;
    }
    public function getLastIdOrder(){
        $stmt = $this->con->prepare("SELECT id_order from dataorder ORDER BY id_order DESC limit 1");
        $stmt->execute();
        $lastid = 0;
        $data = $stmt->fetchAll();
        foreach ($data as $key => $value) {
          $lastid = $data[$key]['id_order'];
        }
        return $lastid;
    }
    //semuausers
    public function getUsers(){
        $stmt = $this->con->prepare("SELECT * FROM users");
        $stmt->execute();
        $res = array();
        $res['result'] = true;
        $res['data'] = $stmt->fetchAll();
        return $res;
    }
    //detail karyawan (rwiayat,pendidikan,pelatihan,pengalaman)
    public function getDetailProfile($nik,$todo){
      if ($todo==1) {
        $sql = "SELECT*FROM riwayatpend WHERE riwayat_nik=:nik";
      }elseif ($todo==2) {
        $sql = "SELECT*FROM keluargakar WHERE nik_kel=:nik";
      }elseif ($todo==3) {
        $sql = "SELECT*FROM kursus WHERE nik_kursus=:nik";
      }else {
        $sql = "SELECT*FROM pengalaman WHERE nik_peng=:nik";
      }
      $stmt = $this->con->prepare($sql);
      $stmt->execute(array(':nik' => $nik));
      return $stmt->fetchAll();
    }
    //profile karyawan
    public function getProfile($nik){
        $data = array();
        $sql = "SELECT*FROM karyawan JOIN jabatan JOIN subdivisi JOIN divisi ON karyawan.id_jabatan = jabatan.id_jabatan AND karyawan.id_ksubdiv = subdivisi.id_subdiv AND subdivisi.id_div = divisi.id_div WHERE nik=:nik";
        $stmt = $this->con->prepare($sql);
        $stmt->execute(array(':nik' => $nik));
        $data['informasi'] = $this->imagePath($stmt->fetchAll(),'photo','karyawan');
        $data['riwayat'] = $this->getDetailProfile($nik,1);
        $data['keluarga'] = $this->getDetailProfile($nik,2);
        $data['sertifikasi'] = $this->getDetailProfile($nik,3);
        $data['pengalaman'] = $this->getDetailProfile($nik,4);
        $res = array();
        $res['result'] = true;
        $res['message'] = "Berhasil Mendapatka Data Profile";
        $res['data'] = $data;
        return $res;
    }
    //subdivisi
    public function getSubdiv(){
        $sql = "SELECT*FROM subdivkor JOIN subdivisi ON subdivkor.id_subdiv = subdivisi.id_subdiv ";
        $stmt = $this->con->prepare($sql);
        $stmt->execute();
        $res = array();
        $res['result'] = true;
        $res['message'] = 'Berhasil Mendapatkan Data Subdvi Order';
        $res['data'] = $this->imagePath($stmt->fetchAll(),'img_holder','subdiv');
        return $res;
    }
    //kategoriatk
    public function getKategori(){
        $sql = "SELECT kategoriatk.* ,(select count(*) from atk where atk.id_kateg = kategoriatk.id_kat) as total FROM kategoriatk,atk WHERE kategoriatk.id_kat=atk.id_kateg GROUP BY kategoriatk.id_kat";
        $stmt = $this->con->prepare($sql);
        $stmt->execute();
        $res = array();
        $res['result'] = true;
        $res['message'] = 'Berhasil Mendapatkan Data Kategori Barang';
        $res['data'] = $this->imagePath($stmt->fetchAll(),'photo','atk');
        return $res;
    }
    //jenisorder
    public function getJenisOrder($idsub){
        if ($idsub==0) {
          $sql = "SELECT * from jenisorder JOIN subdivisi ON jenisorder.id_subdiv = subdivisi.id_subdiv ";
        }else{
          $sql = "SELECT * from jenisorder JOIN subdivisi ON jenisorder.id_subdiv = subdivisi.id_subdiv WHERE jenisorder.id_subdiv = :ids ";
        }
        $stmt = $this->con->prepare($sql);
        if ($idsub==0) {
          $stmt->execute();
        }else{
          $stmt->execute(array(':ids' => $idsub));
        }
        $res = array();
        $res['result'] = true;
        $res['message'] = "Berhasil Mendapatkan Data";
        $res['data'] = $stmt->fetchAll();
        return $res;
    }
    //barangatk
    public function getBarang($idkat){
        if ($idkat==0) {
          $sql = "SELECT*FROM atk JOIN kategoriatk ON atk.id_kateg = kategoriatk.id_kat";
        }else{
          $sql = "SELECT*FROM atk JOIN kategoriatk ON atk.id_kateg = kategoriatk.id_kat where atk.id_kateg = :idk";
        }
        $stmt = $this->con->prepare($sql);
        if ($idkat==0) {
          $stmt->execute();
        }else{
          $stmt->execute(array(':idk' => $idkat));
        }
        $res = array();
        $res['result'] = true;
        $res['message'] = "Berhasil Mendapatkan Data Barang";
        $res['data'] = $stmt->fetchAll();
        return $res;
    }

    public function getSimpleProfile($nik){
        $data = array();
        $sql = "SELECT*FROM karyawan JOIN jabatan JOIN subdivisi JOIN divisi ON karyawan.id_jabatan = jabatan.id_jabatan AND karyawan.id_ksubdiv = subdivisi.id_subdiv AND subdivisi.id_div = divisi.id_div WHERE nik=:nik";
        $stmt = $this->con->prepare($sql);
        $stmt->execute(array(':nik' => $nik));
        return $this->imagePath($stmt->fetchAll(),'photo','karyawan');
    }

    public function getDetailOrder($nik,$idorder){
        $sql = "SELECT*FROM dataorder JOIN jenisorder JOIN subdivisi JOIN subdivkor ON dataorder.id_subdivorder = subdivkor.id_subdiv AND dataorder.id_jenisorder = jenisorder.id_jenisorder AND dataorder.id_subdivorder = subdivisi.id_subdiv WHERE dataorder.nik=:nik and dataorder.id_order=:ido ORDER BY dataorder.id_order DESC ";
        $stmt = $this->con->prepare($sql);
        $stmt->execute(array(':nik' => $nik, ':ido' => $idorder));
        $res = array();
        $res['result'] = true;
        $res['message'] = "Berhasil Mendapatkan Data";
        $data = $stmt->fetchAll();
        foreach ($data as $key => $value) {
          $data[$key]['kordinator'] = $this->getSimpleProfile($data[$key]['nik_kor']);
        }
        $res['data'] = $data;
        return $res;
    }

    public function getHistory($todo,$nik){
        if ($todo=="bon") {
          $sql = "SELECT*FROM bon_atk JOIN atk ON bon_atk.id_atkb = atk.id_atk WHERE id_karya=:nik ORDER BY bon_atk.tgl_pinjam DESC ";
        }else{
          $sql = "SELECT*FROM dataorder JOIN jenisorder JOIN subdivisi JOIN subdivkor ON dataorder.id_subdivorder = subdivkor.id_subdiv AND dataorder.id_jenisorder = jenisorder.id_jenisorder AND dataorder.id_subdivorder = subdivisi.id_subdiv WHERE dataorder.nik=:nik ORDER BY dataorder.id_order DESC ";
        }
        $stmt = $this->con->prepare($sql);
        $stmt->execute(array(':nik' => $nik));
        $res = array();
        $res['result'] = true;
        $res['message'] = "Berhasil Mendapatkan Data";
        $res['data'] = $stmt->fetchAll();
        return $res;
    }

    public function addBon($idatk,$jumlah,$nik){
        $sql = "INSERT INTO bon_atk (id_atkb, jumlahpinjam, id_karya) VALUE (:id, :jml, :nik)";
        $stmt = $this->con->prepare($sql);
        $data = [
          ":id" => $idatk,
          ":jml" => $jumlah,
          ":nik" => $nik
        ];
        $res = array();
        if($stmt->execute($data)){
          $res['result'] = true;
          $res['message'] = "Succes Menambahkan Data Bon Atk";
        }else{
          $res['result'] = false;
          $res['message'] = "Gagal Menambahkan Data Bon Atk";
        }
        return $res;
    }

    public function addOrder($idjenis,$idsub,$nik,$keterangan){
        $sql = "INSERT INTO dataorder(nik,id_jenisorder,id_subdivorder,keterangan) VALUE (:nik, :idj, :ids, :ket)";
        $stmt = $this->con->prepare($sql);
        $data = [
          ":idj" => $idjenis,
          ":ket" => $keterangan,
          ":ids" => $idsub,
          ":nik" => $nik
        ];
        $res = array();
        if($stmt->execute($data)){
          $res['result'] = true;
          $res['message'] = $this->getLastIdOrder()."-Succes Menambahkan Data Order";
          // $res['idmasuk'] = $this->getLastIdOrder();
        }else{
          $res['result'] = false;
          $res['message'] = "Gagal Menambahkan Data Order";
        }
        return $res;
    }

}

?>
